module gitlab.com/gauntletwizard/bazel-go

require (
	github.com/mattn/go-sqlite3 v1.10.0
	google.golang.org/grpc v1.19.1
)
