package main

import (
	"context"
	"fmt"

	"google.golang.org/grpc"

	"github.com/gauntletwizard/bazel-go/network/proto"
)

func main() {
	fmt.Println("vim-go")
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()

	client := proto.NewHelloServiceClient(conn)
	resp, err := client.Hello(context.Background(), &proto.HelloUser{Name: "Bob"})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp.Greeting)
}
