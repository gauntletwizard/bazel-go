package main

import (
	"context"
	"fmt"

	pb "github.com/gauntletwizard/bazel-go/network/proto"
)

type HelloServer struct {
}

func (s HelloServer) Hello(ctx context.Context, user *pb.HelloUser) (*pb.HelloResponse, error) {
	fmt.Printf("Got request from %s.\n", user.Name)
	greeting := fmt.Sprintf("Hello, %s!", user.Name)
	resp := pb.HelloResponse{Greeting: greeting}
	return &resp, nil
}
