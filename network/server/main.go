package main

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"github.com/gauntletwizard/bazel-go/network/proto"
)

func main() {
	fmt.Println("vim-go")

	lis, _ := net.Listen("tcp", fmt.Sprintf("localhost:%d", 8080))
	grpcServer := grpc.NewServer()
	proto.RegisterHelloServiceServer(grpcServer, HelloServer{})
	reflection.Register(grpcServer)
	grpcServer.Serve(lis)
}
