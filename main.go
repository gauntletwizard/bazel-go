package main

import (
	"fmt"
	"github.com/gauntletwizard/bazel-go/proto"
)

func main() {
	qux := proto.Foo{}
	fmt.Println("Hello, World!", qux)
}
