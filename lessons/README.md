This bit of the repo is meant to turn the various subprograms here into a consistent, well documented "Book" that can be read with each chapter being a few commits. Starting from a base template down to a bunch of chapters.

Basic theory of working:
  Each chapter consists of a lesson plan, then a script that generates the commits that make it up. Starting wtih a bunch of git

Should look something like this:
    git checkout -b lesson1 
    git cherry-pick -n 1 2 3 4
    git commit -a < commit1.msg
    git cherry-pick -n 5 6 7 8
    git commit -a < commit2.msg
    git checkout -b lesson2

Log format for picking:
    git log --name-only --abbrev-commit
Check that all commits made it in:
    git log  --abbrev-commit --format=%h
Get list of all commits in the bookscript:
    awk '/git cherry-pick -n/ { n=split($0, line, " "); for (i=4; i<=n; i++) {printf("%s\n",line[i]);}}' < bookscript.sh
