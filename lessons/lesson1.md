# 1 - Getting started with Bazel.

This tutorial assumes you've already got familiarity with Golang - I suggest the [Tour of Go](https://tour.golang.org/welcome/1) if you're not already familiar.

First, we'll need to install Bazel. Refer to the [Installation Guide](https://docs.bazel.build/versions/master/install.html)

Next up, let's create a simple program. An example hello world program is provided. You can run it as you're familiar with: Simply run `go run main.go`

Now, let's build it with Bazel. For this, we'll need a build file. This has also been provided. From your directory, type `bazel run :main`


