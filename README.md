Golang sample bazel build dir.

# Develop
Use go.mod for deps, but build with Bazel
    # Add a go dep simply by importing it. Go mod will automatically import it on invocation.
    go mod tidy
    bazel run //:gazelle -- update-repos --from_file=go.mod

## Lint
run go fmt
    # Format BUILD files:
    bazel run //:gazelle --

# Learning to use this directory:
Build a Hello World program with Bazel
Build a Hello World Network RPC with Bazel
Add a dependency to Bazel and import it
Add a dependency from another bazel workspace.
Build mocks and embedded files with go_rules extras

#On clone:
### Change gazelle annotation
There's a line in BUILD like:
    # gazelle:prefix github.com/gauntletwizard/bazel-go
Change this line to the name of your repository. Your name should be a full URL where your library will be canonically. It is also recommended to create a symlink from ${GOPATH}/src/YOUR/REPO to this directory for compatibility with 'go build'
###Install git hooks
