# Rules for golang support
load("@io_bazel_rules_go//go:def.bzl", "go_binary", "go_library")

# End golang support
# Rules to build docker images
load("@io_bazel_rules_docker//go:image.bzl", "go_image")
load("@io_bazel_rules_docker//container:container.bzl", "container_push")
# End docker support

# Rules to run Gazelle
# gazelle:prefix github.com/gauntletwizard/bazel-go
# gazelle:build_file_name BUILD,BUILD.bazel
load("@bazel_gazelle//:def.bzl", "gazelle")

gazelle(name = "gazelle")
# End Gazelle

# The go_library rule is automatically built by Gazelle - Updated with the files in the library, and the necessary dependencies.
go_library(
    name = "go_default_library",
    srcs = ["main.go"],
    importpath = "github.com/gauntletwizard/bazel-go",
    visibility = ["//visibility:private"],
    deps = ["//proto:go_default_library"],
)

# The go_binary rule creates an executable from the library. In this case, it does not include any files of it's own, just embeds the library
go_binary(
    name = "default",
    embed = [":go_default_library"],
    importpath = "github.com/gauntletwizard/bazel-go",
    visibility = ["//visibility:public"],
)

# The go_image rule should be exactly like the go_binary rule, except that it specifies the arch and OS for building a standard docker image.
go_image(
    name = "dockerimage",
    embed = [":go_default_library"],
    goarch = "amd64",
    goos = "linux",
    importpath = "github.com/gauntletwizard/bazel-go",
    pure = "on",
    visibility = ["//visibility:public"],
)

# The push_dockerimage rule is a support rule to make building and pushing a new docker image one-step
container_push(
    name = "push_dockerimage",
    format = "Docker",
    image = "dockerimage",
    registry = "registry.gitlab.com",
    repository = "gauntletwizard/bazel-go",
    tag = "{BUILD_EMBED_LABEL}",
)
