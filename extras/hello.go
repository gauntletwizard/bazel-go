package main

import (
	"fmt"
	"strings"
	"text/template"

	hello "github.com/gauntletwizard/bazel-go/extras/templates"
)

type greeter struct {
	Name string
}

func main() {
	fmt.Println("Hello World!")
	g := greeter{"Ted"}
	t := template.Must(template.New("hello").Parse(hello.Data))
	buffer := &strings.Builder{}
	t.Execute(buffer, g)
	fmt.Println(buffer.String())
	buffer.Reset()
	t2 := template.Must(template.New("helloMap").Parse(hello.MapData["hello.gotemplate"]))
	t2.Execute(buffer, g)
	fmt.Println(buffer.String())
}
