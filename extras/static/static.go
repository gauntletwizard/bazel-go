package static

import (
	"bytes"
	"net/http"
	"os"
	"path"
	"time"
)

/*
func init() {
	for filename := range Data {
		fmt.Println("Loaded static file", filename)
	}
}
*/

// These constant describe the os.FileMode we use for our fake files
const fileMode = 0444
const dirMode = 0555 | os.ModeDir

// Fileserver is a convenience wrapper around http.FileServer(NewStaticFS(root))
func FileServer(root map[string][]byte) http.Handler {
	return http.FileServer(NewStaticFS(root, ""))
}

// StaticFS implements http.FileSystem for use embedding static data in your binary and serving with http.FileServer
type StaticFS struct {
	root map[string][]byte
	dirs map[string]*staticDir
	// Prefix is an optional (i.e. can be "") string that is stripped from all files in root (or rather, prefixed to all open calls)
	prefix string
}

// NewStaticFS creates a StaticFS from the provided map.
// Once created, ownership of the provided map passes to the StaticFS - No further mutations are allowed.
// prefix is an optional (i.e. can be "") string that is stripped from all files in root.
func NewStaticFS(root map[string][]byte, prefix string) (fs StaticFS) {
	fs.root = root
	fs.prefix = prefix
	fs.dirs = make(map[string]*staticDir)
	for filename := range root {
		dirname := path.Dir(filename)
		dir, ok := fs.dirs[dirname]
		if !ok {
			dir = &staticDir{fs: &fs}
			fs.dirs[dirname] = dir
		}
		dir.files = append(dir.files, filename)
	}
	return
}

func (fs StaticFS) Open(name string) (http.File, error) {
	if data, ok := fs.root[fs.prefix+name]; ok {
		return newStaticFile(data), nil
	}
	if dir, ok := fs.dirs[name]; ok {
		return dir, nil
	}
	return nil, os.ErrNotExist
}

func (fs StaticFS) Stat(name string) (os.FileInfo, error) {
	f, err := fs.Open(name)
	if err != nil {
		return nil, err
	}
	return f.Stat()
}

// StaticDir is an index of all of the files within the StaticFS root. It implements http.File.
type staticDir struct {
	fs *StaticFS
	*bytes.Reader
	files []string
}

func (d staticDir) Readdir(n int) (info []os.FileInfo, e error) {
	for i, name := range d.files {
		f, err := d.fs.Stat(name)
		if err != nil {
			return nil, err
		}
		info = append(info, f)
		if n > 0 && i >= n {
			return
		}
	}
	return nil, nil
}

func (d staticDir) Close() error {
	return nil
}

func (d staticDir) Stat() (os.FileInfo, error) {
	return staticInfo{
		name: path.Base(d.files[0]),
		size: 0,
		mode: dirMode,
	}, nil
}

// staticFile is a wrapper around []byte to implement http.File for use by .
type staticFile struct {
	*bytes.Reader
	name string
}

func newStaticFile(b []byte) (f staticFile) {
	f.Reader = bytes.NewReader(b)
	return
}

func (f staticFile) Readdir(n int) ([]os.FileInfo, error) {
	return nil, os.ErrInvalid
}

func (f staticFile) Close() error {
	return nil
}

func (f staticFile) Stat() (os.FileInfo, error) {
	return staticInfo{
		name: f.name,
		size: int64(f.Len()),
		mode: fileMode,
	}, nil
}

// staticInfo implements os.FileInfo for our filesystem.
type staticInfo struct {
	name string
	size int64
	mode os.FileMode
}

func (i staticInfo) Name() string {
	return i.name
}

func (i staticInfo) Size() int64 {
	return i.size
}

func (i staticInfo) Mode() os.FileMode {
	return i.mode
}

func (i staticInfo) ModTime() time.Time {
	return time.Time{}
}

func (i staticInfo) IsDir() bool {
	return i.mode.IsDir()
}

func (i staticInfo) Sys() interface{} {
	return nil
}
