# Embedded Static Fileserver in Go
This package provides a simple shim between a mapping of files to their data (map[string][]byte, such as provided by a bazel `go_embed_data` rule) and `net/http`'s Fileserver handler. 

## Usage
See the example in `example` for basic usage. Essentially, you need to create an appropriate Map by invoking the `go_embed_data` rule (remember to create a `go_library` to wrap it), then pass that into 'static.Fileserver'. You probably want to wrap that in a `http.StripPrefix` handler, as well.
