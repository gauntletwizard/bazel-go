package main

import (
	"net/http"

	"github.com/gauntletwizard/bazel-go/extras/static"
	"github.com/gauntletwizard/bazel-go/extras/static/testdata"
)

func main() {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(static.NewStaticFS(testdata.Data, "extras/static/testdata/files"))))
	http.ListenAndServe(":8080", nil)
}
