package static

import (
	"fmt"
	//	"net/http"
	//	"os"
	"testing"
)

const foo_name = "foo"
const foo_data = "Test String"

var testData = map[string][]byte{
	foo_name:  []byte(foo_data),
	"bar/baz": []byte("Another test string"),
	"bar/qux": []byte("One more Test string"),
}

func TestStaticFS(t *testing.T) {
	fmt.Println(testData)

	fs := NewStaticFS(testData, "")
	foo, err := fs.Open(foo_name)
	if err != nil {
		t.Fatal(err)
	}
	var b = make([]byte, len(foo_data))
	n, err := foo.Read(b)
	if err != nil {
		t.Fatal(err)
	}
	if n != len(foo_data) {
		t.Fatal("Short read from file foo")
	}

}
