package templates

/*
 * This file takes a go_embed_data rule and compiles all of the templates within
 */

import (
	// Pick your template by uncommenting the appropriate line
	// "text/template"
	"html/template"
)

var Templates map[string]*template.Template

func init() {
	Templates = make(map[string]*template.Template, len(MapData))
	for name, t := range MapData {
		Templates[name] = template.Must(template.New(name).Parse(t))
	}
}
